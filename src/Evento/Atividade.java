package Evento;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import Persistencia.Dados;

public class Atividade extends Evento {

	private Scanner tecString = new Scanner(System.in);
	private Scanner tecInt = new Scanner(System.in);
	private static int id = 0;
	private String ministrante;
	private ArrayList<Pessoa> participantes = new ArrayList<Pessoa>();
	private ArrayList<Recurso> recursosNec = new ArrayList<Recurso>();
	private ArrayList<Recurso> recursosPendentes = new ArrayList<Recurso>();
	private static Evento evento;
	private static int totalInscritosAtividade;

	/*
	 * CONSTRUTORES
	 */

	/*
	 * M�TODOS CONCRETOS
	 */
	// *CRIADO* M�todo usado pelo ministrante para solicitar recursos
	public void solicitaRecurso() throws IOException {
		// Evento evento = carregaEvento();

		System.out.println("A lista abaixo cont�m todos os recursos dispon�veis para a Atividade");
		System.out.println("Digite o c�digo do recurso que deseja solicitar");
		Recurso rs = selecionaRecurso(); // rs = recurso selecionado

		System.out.println("Digite a quantidade necess�ria: ");
		int qnt0 = tecInt.nextInt();
		int qnt1 = rs.getQuantidade();
		int result = qnt1 - qnt0;
		if (rs.getQuantidade() <= 0) {
			System.out.println("O recurso " + rs.getNome() + " n�o est� dipon�vel, aguarde o contato do administrador! ");
			recursosPendentes.add(rs);
		} else {
			if (result  < 0){
				result = Math.abs(result); //convertendo para positivo
				rs.setQuantidade(result); //setando o quantidade que ficou pendente
				System.out.println("N�o existe quantidade suficiente para o recurso " + rs.getNome() + "\n"
						+ "aguarde o contato do administrador!\n ");
				
				recursosPendentes.add(rs);
			}
		}
		rs.setQuantidade(qnt1);
		recursosNec.add(rs);
	}

	public void exibeParticipantes() {
		for (Pessoa p : participantes) {
			System.out.println(p);
		}
	}

	// *FALTA* testar
	public ArrayList<Recurso> getRecursosNec() {
		/*
		 * for(Recurso r: recursosNec){ System.out.println(r); }
		 */

		return recursosNec;
	}

	public void setRecursosNec(ArrayList<Recurso> recursosNec) {
		this.recursosNec = recursosNec;
	}

	public String getMinistrante() {
		return ministrante;
	}

	public void setMinistrante(String ministrante) {
		this.ministrante = ministrante;
	}

	public int getId() {
		return id;
	}

	public static void setId(int id) {
		Atividade.id++;
	}

	public int incremetaTotalInscritosAtividade() {
		setTotalInscritosAtividade();
		return getTotalInscritosAtividade();
	}

	public int getTotalInscritosAtividade() {
		return Atividade.totalInscritosAtividade;

	}

	public void setTotalInscritosAtividade() {
		Atividade.totalInscritosAtividade++;
	}

	public ArrayList<Recurso> getRecursosPendentes() {
		return recursosPendentes;
	}

	public void setRecursosPendentes(ArrayList<Recurso> recursosPendentes) {
		this.recursosPendentes = recursosPendentes;
	}

	@Override
	public String toString() {
		return getId() + "|" + getNome() + "|" + getDescricao() + "|" + getMinistrante() + "|" + getLocal() + "|"
				+ getTipo() + "|" + getTotalDeVagas() + "|" + getTotalInscritos() + "|" + getVagasDisponives() + "|"
				+ getDataHoraIncio() + "|" + getDataHoraFim();
	}
}
