package Evento;
import java.util.ArrayList;

public class Pessoa {
	
	
	private String nome;
	private int numInscricao;
	private String cidade;
	private String bairro;
	private String logradouro;
	private String num;
	private String telefone;
	private String email;
	private String apelido; //� o que aparece no crach�
	private String formacaoAcademica; //todos recebem forma��o academica
	private String biografia; //Participante, organizador e imprensa n�o recebem biografia
	private String tipoParticipante; //Participante, Ministrante ou Organizardor
	private static int totalInscritos = 0;
	
	ArrayList<Atividade> atividades = new ArrayList<Atividade>();
	
	//private Scanner tecString = new Scanner(System.in);
	//private Scanner tecInt = new Scanner(System.in);
	
	/*
	 * CONSTRUTORES
	 */
	
	/*
	 * M�TODOS CONCRETOS 
	 */
	public void listaAtividadesDoInscrito(){
		for(Atividade a: atividades){
			System.out.println(a);
		}
	}
	
	/*
	 * GETTERS E SETTERS 
	 */
	
	public String getNome() {
		return nome;
	}
	
	public int getNumInscricao() {
		return numInscricao;
	}

	public void setNumInscricao(int numInscricao) {
		this.numInscricao = numInscricao;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCidade() {
		return cidade;
	}
	
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	
	public String getBairro() {
		return bairro;
	}
	
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	
	public String getLogradouro() {
		return logradouro;
	}
	
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	public String getNum() {
		return num;
	}
	
	public void setNum(String num) {
		this.num = num;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getApelido() {
		return apelido;
	}
	
	//*FALTA* limitar o n�mero de caracteres e lan�ar exception
	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	
	public String getFormacaoAcademica() {
		return formacaoAcademica;
	}
	
	public void setFormacaoAcademica(String formacaoAcademica) {
		this.formacaoAcademica = formacaoAcademica;
	}
	
	public String getBiografia() {
		return biografia;
	}
	
	public void setBiografia(String biografia) {
		this.biografia = biografia;
	}
	
	public String getTipoParticipante() {
		return tipoParticipante;
	}
	
	public void setTipoParticipante(String tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}
	
	public ArrayList<Atividade> getEventos() {
		return atividades;
	}
	
	public void setEventos(ArrayList<Atividade> eventos) {
		this.atividades = eventos;
	}
	
	public static int getTotalInscritos() {
		return totalInscritos;
	}
	
	//*PRONTO* Incrementa o total de inscritos
	/*
	public static void setTotalInscritos() {
		Pessoa.totalInscritos++;
	}
	*/
	
	@Override
	public String toString(){
		return getNumInscricao() +
				"|" + getNome() +
				"|" + getApelido() +
				"|" + getEmail() +
				"|" + getTelefone() +
				"|" + getTipoParticipante()+
				"|" + getBiografia()+
				"|" + getCidade() +
				"|" + getBairro() +
				"|" + getLogradouro() +
				"|" + getNum(); 
	}
	
}