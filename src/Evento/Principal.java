package Evento;

import java.awt.KeyEventPostProcessor;
import java.io.IOException;
import java.util.Scanner;

import com.sun.glass.events.KeyEvent;

import Persistencia.Dados;

public class Principal {

	private static int opcao = 0;
	private static Scanner tecString = new Scanner(System.in);
	private static Scanner tecInt = new Scanner(System.in);

	static Evento evento = new Evento();
	static Atividade ativ = new Atividade();

	// *PRONTO* Menu Principal para o ORGANIZADOR
	public static void menuPrincipal() throws IOException {
		try {
			Scanner tecInt = new Scanner(System.in);
			System.out.println("MENU > PRINCIPAL\n");
			System.out.println("---------------------------------------------------\n");
			System.out.println("Digite:");
			System.out.println("        1: Inscri��es.\n" 
					+ "        2: Submeter palestra, oficina, etc.\n"
					+ "        3: Gerenciar evento.\n"
					+ "        9: SAIR DO SISTEMA\n");

			System.out.printf("Resposta: ");
			opcao = tecInt.nextInt();
			switch (opcao) {
			case 1:
				evento.inscricaoParticipante();
				break;
			case 2:
				evento.submeterAtividade();
				break;
			case 3:
				menuGerenciaEvento();
				break;
			case 9:
				System.out.println("\n=============================================\n"
							+ "Sistema ENCERRADO! At� logo!");
				System.exit(0);
				break;
			default:
				System.out.println("\n ----| Op��o inv�lida! Tente novamente:|----\n");
				menuPrincipal();
				break;
			}
		} catch (Exception e) {
			System.out.println("Voc� digitou uma op��o inv�lida! Tente novamente!\n");
			menuPrincipal();
		}

	}

	// *PRONTO* Menu Principal para usu�rios n�o cadastrados como participantes
	// e ministrantes
	public static void menuPrincipalPart() throws IOException {
		try {
			Scanner tecInt = new Scanner(System.in);
			System.out.println("MENU > PRINCIPAL\n");
			System.out.println("---------------------------------------------------\n");
			System.out.println("Digite:");
			System.out.println("        1: Inscri��es.\n" 
			+ "        2: Submeter palestra, oficina, etc.\n"
			+ "        9: SAIR DO SISTEMA\n");

			System.out.printf("Resposta: ");
			opcao = tecInt.nextInt();
			switch (opcao) {
			case 1:
				evento.inscricaoParticipante();
				break;
			case 2:
				evento.submeterAtividade();
				break;
			case 9:
				System.out.println("\n=============================================\n"
						+ "Sistema ENCERRADO! At� logo!");
				System.exit(0);
				break;
			default:
				System.out.println("\n ----| Op��o inv�lida! Tente novamente: |----\n");
				menuPrincipalPart();
				break;
			}
		} catch (Exception e) {
			System.out.println("Voc� digitou uma op��o inv�lida! Tente novamente!\n");
			menuPrincipalPart();
		}
	}

	// *PRONTO* SubMenu para Gereciar Recursos
	public static void menuGerenciaRecurso() throws IOException {
		try {
			Scanner tecInt = new Scanner(System.in);
			System.out.println("MENU > PRINCIPAL > GERENCIAR EVENTO > GERENCIAR RECURSOS\n");
			System.out.println("---------------------------------------------------\n");
			System.out.println("Digite:");
			System.out.println("        1: Novo recurso.\n"
					+ "        2: Exibir lista de todos os recursos cadastrados.\n"
					+ "        3: Aumentar o estoque de um recuso.\n" + "        4: Reduzir o estoque de um recurso.\n"
					+ "        5: Excluir um recurso.\n" + "        0: VOLTAR AO MENU GERENCIAR EVENTO\n"
					+ "        9: SAIR DO SISTEMA\n");

			System.out.printf("Resposta: ");
			opcao = tecInt.nextInt();

			switch (opcao) {
			case 1:
				evento.cadastraRecurso();
				break;
			case 2:
				evento.exibeTodosOsRecursos();
				break;
			case 3:
				evento.incrementaEstoque();
				break;
			case 4:
				evento.reduzEstoque();
				break;
			case 5:
				evento.excluiRecurso();
				break;
			case 9:
				System.out.println("\n=============================================\n"
							+ "Sistema ENCERRADO! At� logo!");
				System.exit(0);
				break;
			case 0:
				menuGerenciaEvento();
				break;
			default:
				System.out.println("\n ----| Op��o inv�lida! Tente novamente: |----\n");
				menuGerenciaRecurso();
				break;
			}
		} catch (Exception e) {
			System.out.println("Voc� digitou uma op��o inv�lida! Tente novamente!\n");
			menuGerenciaRecurso();
		}

	}

	public static void menuGerenciaEvento() throws IOException {

		try {
			Scanner tecInt = new Scanner(System.in);
			System.out.println("MENU > PRINCIPAL > GERENCIAR EVENTO\n");
			System.out.println("---------------------------------------------------\n");
			System.out.println("Digite:");
			System.out.println("        1: Criar Evento \n" + "        2: Remover Atividade Aprovada.\n"
					+ "        3: Exibir Atividades Aprovadas.\n" + "        4: Gerenciar Recursos.\n"
					+ "        5: Gerenciar submiss�es.\n" + "        6: Ver lista de inscritos.\n"
					+ "		   7: Exibir Evento Principal.\n" + "        0: VOLTAR AO MENU PRINCIPAL\n"
					+ "        9: SAIR DO SISTEMA\n");

			System.out.printf("Resposta: ");
			opcao = tecInt.nextInt();
			switch (opcao) {
			case 1:
				evento.eventoExiste();
				break;
			case 2:
				evento.excluiAtividadeAprovada();
				break;
			case 3:
				evento.exibeTodasAtividadesAprovadas();
				break;
			case 4:
				menuGerenciaRecurso();
				break;
			case 5:
				menuGerenciaSubmissoes();
				break;
			case 6:
				evento.exibeInscritosEvento();
				break;
			case 7:
				evento.exibeEvento();
				break;
			case 9:
				System.out.println("\n=============================================\n"
							+ "Sistema ENCERRADO! At� logo!");
				System.exit(0);
				break;
			case 0:
				menuPrincipal();
				break;
			default:
				System.out.println("\n ----| Op��o inv�lida! Tente novamente: |----\n");
				menuGerenciaRecurso();
				break;
			}
		} catch (Exception e) {
			System.out.println("Voc� digitou uma op��o inv�lida! Tente novamente!\n");
			menuGerenciaRecurso();
		}

	}

	public static void menuGerenciaSubmissoes() throws IOException {

		try {
			Scanner tecInt = new Scanner(System.in);
			System.out.println("MENU > PRINCIPAL > GERENCIAR EVENTO > GERENCIAR SUBMISS�ES\n");
			System.out.println("---------------------------------------------------\n");
			System.out.println("Digite:");
			System.out.println(
					"        1: Aceitar submiss�es.\n" + "        2: Ver todas as submiss�es que aguardam aprova��o.\n"
							+ "        0: VOLTAR ao menu anterior."
							+ "        9: SAIR DO SISTEMA\n");

			System.out.printf("Resposta: ");
			opcao = tecInt.nextInt();
			switch (opcao) {
			case 1:
				evento.aprovaSubmissao();
				break;
			case 2:
				evento.exibeTodasSubmissoes();
				break;
			case 9:
				System.out.println("\n=============================================\n"
						+ "Sistema ENCERRADO! At� logo!");
				System.exit(0);
				break;
			case 0:
				menuGerenciaEvento();
				break;
			default:
				System.out.println("\n ----| Op��o inv�lida! Tente novamente: |----\n");
				menuGerenciaSubmissoes();
				break;
			}
		} catch (Exception e) {
			System.out.println("Voc� digitou uma op��o inv�lida! Tente novamente!\n");
			menuGerenciaSubmissoes();
		}

	}

	public static void menuEventoExiste() throws IOException {

		try {
			Scanner tecInt = new Scanner(System.in);
			System.out.println("" + "-------------------------------|ATEN��O|------------------------------\n"
					+ "S� � poss�vel gerenciar um evento por vez\n"
					+ "Digite 1: para apagar o evento atual e cadastrar um novo evento agora\n"
					+ "Digite 2: para cancelar e voltar ao GERENCIAR EVENTO\n"
					+ "----------------------------------------------------------------------\n");

			System.out.printf("Resposta: ");
			opcao = tecInt.nextInt();
			switch (opcao) {
			case 1:
				evento.criaEvento();
				break;
			case 2:
				menuGerenciaEvento();
				break;
			default:
				System.out.println("\n--------------------|ERRO|--------------------\n"
						+ "Voc� digitou uma op��o inv�lida e foi direcionado ao menu Gerenciar Evento\n"
						+ "tente novamente!\n\n");

				menuGerenciaEvento();
				break;
			}
		} catch (Exception e) {
			System.out.println("Voc� digitou uma op��o inv�lida! Tente novamente!\n");
		}
	}

	public static void inicializa() throws IOException {
		System.out.println("Carregando o sistema...");
		evento = Dados.carregaEvento();
		evento.setParticipantes(Dados.carregaInscritos());
		evento.setAtividades(Dados.carregaSubmissoesAprovadas());
		evento.setRecursosDisponiveis(Dados.carregaRecursos());
		evento.setSubmissoes(Dados.carregaSubmissoesAguardandoAprovacao());
		System.out.println("Sistema pronto...\n");
		System.out.println("Bem-vindo ao E-VENTOS. Seu gerenciador de eventos!");
		System.out.println("--------------------------------------------------");

	}

	public static void login() throws IOException {
		String user = "org", rUser;
		int pass = 123, rPass;

		try {
			Scanner tecInt = new Scanner(System.in);

			System.out.println("Deseja entrar como administrador? 1 - SIM | 2 - N�o");
			System.out.printf("Resposta: ");
			int resp = tecInt.nextInt();
			if (resp == 1) {
				do {
					System.out.println("========| LOGIN |========");
					System.out.printf("Usu�rio: ");
					rUser = tecString.nextLine();
					System.out.printf("Senha: ");
					rPass = tecInt.nextInt();
					System.out.println("=========================\n");
					if (rPass == pass && rUser.equals(user)) {
						menuPrincipal();
					} else {
						System.out.println("\nUsu�rio e/ou senha n�o conferem!\nTENTE NOVAMENTE!\n");
						login();
					}
				} while (user != rUser && pass != rPass);

			} else if (resp == 2) {
				menuPrincipalPart();
			}
		} catch (Exception e) {
			System.out.println("Op��o inv�lida! Tente novamente!.\n");
			login();
		}
	}

	public static void main(String[] args) throws IOException {

		inicializa();

		login();

	}
}
