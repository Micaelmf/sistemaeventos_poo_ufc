package Evento;

public class Recurso {
	String nome;
	int quantidade;
	
	//M�todos concretos
	public void exibe(){
		System.out.println("------------------");
		System.out.println("Nome: " + getNome() + "\n Quantidade: " + getQuantidade());
	}
	
	//M�todos acessadores e modificadores
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getQuantidade() {
		return quantidade;
	}
	//*OBSERVA��ES* � poss�vel que o resultado seja negativo para n�o interromper as atividades que o sistema se propoe acompanhar.
	//				O tratamento deste erro envolve uma solu��o mais complexa que lan�aria um exce��o.
	public void setQuantidade(int quantidade) {
		if (quantidade < 0){
			System.out.println("ATEN��O! O resultado dessa opera��o ser� negativo.");
		}
		this.quantidade = quantidade;
	}
	
	

	@Override
	public String toString(){
		return getNome() + "|" + getQuantidade(); 
	}

	
	
	
}
