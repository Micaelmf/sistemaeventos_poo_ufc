package Evento;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import Persistencia.Dados;

public class Evento {
	private String nome;
	private String descricao;
	private String coordenador;
	private String local;
	private String tipo;
	private int totalDeVagas; // total de vagas que foram disponibilizadas para o evento
	private static int totalInscritos;
	private int vagasDisponives; // Vagas que est�o sobrando (= totaldeVagas - totalInscritos)
	private String dataHoraIncio;
	private String dataHoraFim;
	private String cidade;
	private String bairro;
	private String logradouro;
	private int num;

	private ArrayList<Pessoa> participantes = new ArrayList<Pessoa>();
	private ArrayList<Recurso> recursosDisponiveis = new ArrayList<Recurso>();
	private ArrayList<Atividade> atividades = new ArrayList<Atividade>();
	private ArrayList<Atividade> submissoes = new ArrayList<Atividade>();

	private Scanner tecString = new Scanner(System.in);
	private Scanner tecInt = new Scanner(System.in);

	static Evento e = new Evento();
	static Atividade ativ = new Atividade();

	// *CRIADO* m�todo feito pra quando for necess�rio obter o valor do evento
	// na classe principal
	public Evento carregaEvento() {
		return Principal.evento;
	}

	// *CRIADO* m�todo feito para pegar o valor do evento na classe principal e
	// guardar no atributo "e" dessa classe
	public void salvaEvento() {
		e = Principal.evento;
	}

	/*
	 * M�TODOS CONCRETOS
	 */

	public void eventoExiste() throws IOException {
		Evento evento = carregaEvento(); // pegando o valor de evento na classe
											// Principal
		if (evento.getNome() == null) {
			// System.out.println("O NOME � VAAAAAAAAAAAZIO!");
			evento.criaEvento();
		} else {
			// System.out.println("O NOME TEM ALGUMA COOOOOOISA");
			Principal.menuEventoExiste();
		}
	}

	public void criaEvento() throws IOException {
		Evento evento = carregaEvento(); // pegando o valor de evento na classe
											// Principal

		try{
			Scanner tecInt = new Scanner(System.in);
			System.out.println("---- INFORMA��ES GERAIS ----");
			System.out.printf("Nome: ");
			evento.setNome(tecString.nextLine());
			System.out.printf("Descri��o do evento: ");
			evento.setDescricao(tecString.nextLine());
			System.out.printf("Coordenador: ");
			evento.setCoordenador(tecString.nextLine());
			System.out.printf("Local (Ex.: Sala 1, Audit�rio, laborat�rio, etc.): ");
			evento.setLocal(tecString.nextLine());
			evento.setTipo("Evento Principal");
			System.out.printf("Total de vagas: ");
			evento.setTotalDeVagas(tecInt.nextInt());
			System.out.println("----| INFORMA��ES DE DATA E HORA |----");
			System.out.printf("Data e hor�rio de in�cio (Ex.: 01/12/16 08:00) : ");
			evento.setDataHoraIncio(tecString.nextLine());
			System.out.printf("Data e hor�rio de encerramento (Ex.: 01/12/16 08:00) : ");
			evento.setDataHoraFim(tecString.nextLine());
			
			// Pedindo endere�o
			System.out.println("---- INFORMA��ES DE ENDERE�O ----");
			System.out.printf("Cidade: ");
			evento.setCidade(tecString.nextLine());
			System.out.printf("Bairro: ");
			evento.setBairro(tecString.nextLine());
			System.out.printf("Logradouro: ");
			evento.setLogradouro(tecString.nextLine());
			System.out.printf("N�mero: ");
			evento.setNum(tecString.nextInt());
			System.out.println("Calculando vagas dispon�veis....");
			evento.vagasDisponives = evento.totalDeVagas;
		}catch(Exception e){
			System.out.println("\n ----| ERRO |----\n! Tente novamente!\n");
			criaEvento();
		}
		

		Dados.salvaEvento(evento);
		// setEvento(Dados.carregaEvento());
		Principal.menuGerenciaEvento();

	}

	public void exibeEvento() throws IOException {
		//System.out.println(carregaEvento()); // mostrando o valor de evento na classe principal
		Evento evento = carregaEvento();
		System.out.println("Nome: " + evento.getNome() + 
				"\nDescri��o: " + evento.getDescricao() +
				"\nCoordenador: " + evento.getCoordenador() +
				"\nEndere�o: " + evento.getLogradouro() + ", " + evento.getNum()+ ", " + evento.getCidade() + "\n" +
				"\nData e hora de IN�CIO: " + evento.getDataHoraIncio() +
				"\nData e hora de FIM: " + evento.getDataHoraFim() +
				"\nTotal de Inscritos: " + getTotalInscritos() +
				"\nTotal de vagas: " + evento.getTotalDeVagas()+ "\n\n");
		Principal.menuGerenciaEvento();
	}

	public boolean verificaVagasEvento() throws IOException {
		Evento evento = carregaEvento(); // pegando o valor de evento na classe Principal
		if (evento.getVagasDisponives() > 0) {
			return true; // se tiver vaga
		} else {
			return false; // se nao tiver vaga
		}
	}

	// *PRONTO* Este m�todo cadastra um novo recurso dispon�vel para o evento
	// *FALTA* Lan�ar exce��o
	public void cadastraRecurso() throws IOException {
		// int resp = 0;
		Recurso recurso = new Recurso();


		try{
			Scanner tecInt = new Scanner(System.in);
			System.out.printf("Nome: ");
			recurso.setNome(tecString.next());
			System.out.printf("Quantidade: ");
			recurso.setQuantidade(tecInt.nextInt());
			
			for(int i = 0; i < recursosDisponiveis.size(); i++){
				if(recursosDisponiveis.get(i).equals(recurso)){
					System.out.println("Este recurso j� existe! O estoque ser� alterado para " + recurso.getQuantidade() + " .");
					recursosDisponiveis.get(i).setQuantidade(recurso.getQuantidade());
					Dados.salvaRecursos(this.recursosDisponiveis);
					Principal.menuGerenciaRecurso();
				}	
			}		
		}catch(Exception e){
			System.out.println("\n ----| ERRO |----\n! Tente novamente!\n");
			cadastraRecurso();
		}
		
		
		this.recursosDisponiveis.add(recurso);// Adiciona o recurso cadastrado no arraylist
		Dados.salvaRecursos(recurso);// Adiciona o recurso cadastrado no arquivo texto
		
		Principal.menuGerenciaRecurso();
	}

	// *PRONTO* Aumenta o estoque de um recurso
	public void incrementaEstoque() throws IOException {
		Recurso recurso = new Recurso();
		//System.out.println("Digite o c�digo do recurso que deseja acrescentar estoque: ");
		recurso = selecionaRecurso();
		
		try{
			Scanner tecInt = new Scanner(System.in);
			for (int i = 0; i < recursosDisponiveis.size(); i++) {
				if (recursosDisponiveis.get(i).equals(recurso)) {
					System.out.printf("Digite a quantidade a ser acrescentada: ");
					int quantidade = tecInt.nextInt();					
					recursosDisponiveis.get(i).setQuantidade(recursosDisponiveis.get(i).getQuantidade() + quantidade);
					Dados.salvaRecursos(this.recursosDisponiveis);
					
					Principal.menuGerenciaEvento();
				}
			}
		}catch(Exception e){
			System.out.println("\n ----| ERRO! |----\n Tente novamente!\n");
			incrementaEstoque();
		}
		
		
	}

	//*PRONTO* Permite que o Organizador reduza o estoque de um determinado recurso.
	public void reduzEstoque() throws IOException{
		Recurso rs = selecionaRecurso();
		
		try{
			Scanner tecInt = new Scanner(System.in);
			for (int i = 0; i < recursosDisponiveis.size(); i++) {
				if (recursosDisponiveis.get(i).equals(rs)) {
					System.out.printf("------------------------------\n");
					System.out.println("\nRecurso: " + rs.getNome());
					System.out.printf("\nEstoque ATUAL: " + rs.getQuantidade()+ "/n");
					System.out.println("------------------------------");				
					System.out.printf("Digite a quantidade a ser reduzida: ");
					int qnt = tecInt.nextInt();					
					recursosDisponiveis.get(i).setQuantidade(recursosDisponiveis.get(i).getQuantidade() - qnt);
	
					System.out.println("NOVO estoque: " + rs.getQuantidade() + "\n");
					Dados.salvaRecursos(this.recursosDisponiveis);
					
					Principal.menuGerenciaRecurso();
				}
			}
		}catch(Exception e){
			System.out.println("\n ----| ERRO! |----\n Tente novamente!\n");
			reduzEstoque();
		}
		System.out.println("Altera��o realizada com sucesso!\n\n");		
	}
	
	//*PRONTO* Reduzir estoque dos recursos solicitados pelo ministrantes ap�s a aprova��o do organizador
	public void decrementaEstoque(Atividade ativ, int indice){
		for(int i = 0; i < recursosDisponiveis.size(); i++ ){
			for(int j = 0; j < ativ.getRecursosNec().size(); j++){
				if(recursosDisponiveis.get(i).equals(ativ.getRecursosNec().get(j))){
					recursosDisponiveis.get(i).setQuantidade(recursosDisponiveis.get(i).getQuantidade() - ativ.getRecursosNec().get(j).getQuantidade());
				}
			}
		}
	}
	
	// *PRONTO* O ORGANIZADOR Exclui um recurso do arraylist recursosDisponiveis<Recurso>
	public void excluiRecurso() throws IOException {
		System.out.println("Digite o c�digo do recurso que deseja excluir: ");
		Recurso recurso = selecionaRecurso();

		recursosDisponiveis.remove(recurso);		
		Dados.salvaRecursos(this.recursosDisponiveis);
		System.out.println("\n----| " + recurso.getNome() + "Foi exclu�do com sucesso!|----\n");
		
		Principal.menuGerenciaRecurso();
	}
	
	// *PRONTO* Sobrecarga
	/*
	public void excluiRecurso(Recurso r) throws IOException {
		if (recursosDisponiveis.contains(r)) {
			recursosDisponiveis.remove(r);
		}
	}
	*/

	public void excluiAtividadeAprovada() throws IOException{
		exibeAtividadesAprovadas();
		
		try{
			Scanner tecInt = new Scanner(System.in);
			System.out.printf("Digite o c�digo da ATIVIDADE que deseja EXCLUIR: ");
			int indice = tecInt.nextInt();

			System.out.println("Voc� realmente deseja excluir? Digite 1- Sim ou 2- N�o");
			int resp = tecInt.nextInt();

			if (resp == 1) {
				this.atividades.remove(indice);
				Dados.salvaSubmissoesAprovadas(this.atividades);
				Principal.menuGerenciaEvento();
			} else if (resp == 2) {
				Principal.menuGerenciaSubmissoes();
			} else {
				System.out.println("\nVoc� uma op��o inv�lida e por seguran�a foi direcionado para o MENU GERENCIAR SUBMISSOES\n");
				Principal.menuGerenciaSubmissoes();
			}
		}catch(Exception e){
			System.out.println("\n ----| ERRO! |----\n Tente novamente!\n");
			excluiAtividadeAprovada();
		}
		
	}
		
	//*PRONTO* Exibe todas a atividades aprovadas
	public void exibeAtividadesAprovadas() throws IOException{
		if(!atividades.isEmpty()){
			for (Atividade a : atividades){
				System.out.println("--------------------------------");
				System.out.println("C�d.:" + atividades.indexOf(a) + "\n" + 
						"Ministrante: " + a.getMinistrante() + "\n" +  
						"T�tulo: " + a.getNome() + "\n" + 
						"Descri��o: " + a.getDescricao() + "\n" +
						"Recursos: " + a.getRecursosNec() + "\n" +
						"Vagas Dispon�veis: " + a.getVagasDisponives() + "\n" +
						"Participantes: " + a.getParticipantes());
				if(!a.getRecursosPendentes().isEmpty()){
					System.out.println("Recursos Pendentes: " + a.getRecursosPendentes());
				}
				
				System.out.println("--------------------------------");
			}
			
			
		}else{
			System.out.println("\n\n----------------| ATEN��O |--------------------\n"
					+ "Nenhuma atividade foi aprovada\n\n");
			Principal.menuGerenciaEvento();
		}
	}
	
	//*PRONTO* Exibe todas a atividades aprovadas
		public void exibeTodasAtividadesAprovadas() throws IOException{
			if(!atividades.isEmpty()){
				for (Atividade a : atividades){
					System.out.println("--------------------------------");
					System.out.println("C�d.:" + atividades.indexOf(a) + "\n" + 
							"Ministrante: " + a.getMinistrante() + "\n" +  
							"T�tulo: " + a.getNome() + "\n" + 
							"Descri��o: " + a.getDescricao() + "\n" +
							"Recursos: " + a.getRecursosNec() + "\n" +
							"Vagas Dispon�veis: " + a.getVagasDisponives() + "\n" +
							"Participantes: " + a.getParticipantes());
					System.out.println("--------------------------------");
				}
				Principal.menuGerenciaEvento();
			}else{
				System.out.println("\n\n----------------| ATEN��O |--------------------\n"
						+ "Nenhuma atividade foi aprovada\n\n");
				Principal.menuGerenciaEvento();
			}
		}
	
	public void submeterAtividade() throws IOException {
		//Evento evento = carregaEvento(); // pegando o valor de evento na classe Principal
		Pessoa pessoa = new Pessoa();
		Atividade atividade = new Atividade();
		
		// Cadastrando Ministrante
		if (verificaVagasEvento() == true) {
			System.out.println("Voc� deve fazer sua inscri��o agora, antes de submeter sua atividade");
			pessoa = inscricaoGenerica(pessoa);
			pessoa.setTipoParticipante("Ministrante");
			System.out.printf("Biografia (Fale um pouco sobre a sua carreira): ");
			pessoa.setBiografia(tecString.nextLine());
			
			boolean repete = false;
			for(Pessoa a : participantes){ //procura se a pessoa j� est� cadastrada
				if(!a.equals(pessoa)){ // se n�o estiver ent�o cadastra
					repete = true;
				}
			}
			if (!repete){
				pessoa.setNumInscricao(Evento.getTotalInscritos());
				System.out.printf("INSCRI��O N�: " + pessoa.getNumInscricao() + "\n");
				participantes.add(pessoa); // adicionando o ministrante no arraylist de participantes
				Dados.salvaInscrito(pessoa); // persiste o inscrito no arquivo texto
				Evento.setTotalInscritos(Evento.getTotalInscritos());
				System.out.println("Inscri��o realizada com sucesso!");									
			}else{
				// Cadastrando Atividade
				System.out.println("Agora vamos submeter sua atividade:\n");
				atividade.setMinistrante(pessoa.getNome());
				System.out.printf("T�tulo: ");
				atividade.setNome(tecString.nextLine());
				System.out.printf("Descri��o do atividade: ");
				atividade.setDescricao(tecString.nextLine());
				System.out.println("Tipo (Ex.:Oficina, F�rum, Abertura, etc): ");
				atividade.setTipo(tecString.nextLine());
				
				int opcao = 1;
				try{
					do {
						System.out.println("Digite: \n" + "1: Solicitar mais um recurso\n" 
									+ "2: Concluir Submiss�o\n");
						opcao = tecInt.nextInt();
						System.out.println("Resposta: ");

						switch (opcao) {
						case 1:
							atividade.solicitaRecurso();					
							break;
						case 2:
							submissoes.add(atividade);
							Dados.salvaSubmissoesAguardandoAprovacao(atividade);
							Dados.salvaRecursosAtividade(atividade.getRecursosNec(),ativ);
							Dados.salvaRecursosPendentesAtividade(atividade.getRecursosPendentes(), ativ);
							submissoes.add(atividade);
							System.out.println("-------------------------------------------");
							System.out.println("SUBMISS�O REALIZADA COM SUCESSO");
							System.out.println("Sua submissao est� sob an�lise. Aguarde o contato do Organizador ");
							System.out.println("-------------------------------------------");
							Principal.menuPrincipalPart();
							break;
						default:
							System.out.println("Op��o inv�lida! Tente novamente!");
							break;
						}
					} while (opcao != 2);

				}catch(Exception e){
					System.out.println("Voc� digiou uma op��o inv�lida");
				}
			}
		} else {
			System.out.println("-----------------| ATEN��O |-----------------\n" 
							+ "Sua inscri��o n�o pode ser realizada\n"
							+ "N�o h� vagas no  Evento\n" 
							+ "---------------------------------------------\n\n");
			Principal.menuPrincipalPart();
		}


		//---------------------------------

	}

	//*PRONTO* Lista todas as submiss�es
	public void exibeTodasSubmissoes() throws IOException{
		boolean vazio = true;
		for (Atividade a: submissoes){
			System.out.println("--------------------------------");
			System.out.println("C�d.:" + submissoes.indexOf(a) + "\n" + 
					"Ministrante: " + a.getMinistrante() + "\n" +  
					"T�tulo: " + a.getNome() + "\n" + 
					"Descri��o: " + a.getDescricao() + "\n" +
					"Recursos: " + a.getRecursosNec() + "\n" +
					"Participantes: " + a.getParticipantes());
			System.out.println("--------------------------------");
			vazio = false;
		}
		
		if(vazio == true){
			System.out.println("\n----------| ATEN��O |----------\nA lista est� vazia! ");
		}
		
		Principal.menuGerenciaSubmissoes();
	}
	
	//*PRONTO* Lista todas as submiss�es
	public boolean exibeSubmissoes() throws IOException{
		
		if (submissoes.isEmpty()){
			return false;
		}else{
			for (Atividade a: submissoes){
				System.out.println("--------------------------------");
				System.out.println("C�d.:" + submissoes.indexOf(a) + "\n" + 
						"Ministrante: " + a.getMinistrante() + "\n" +  
						"T�tulo: " + a.getNome() + "\n" + 
						"Descri��o: " + a.getDescricao() + "\n" +
						"Recursos: " + a.getRecursosNec() + "\n" +
						"Participantes: " + a.getParticipantes());
				System.out.println("--------------------------------");
				return true;
			}
		}
		return true;
	}
	
	//*CRIADO* M�todo seleciona a atividade que ser� aprovada e aprova
	public void aprovaSubmissao() throws IOException{		
		int indice = 0;
		try{
			Scanner tecInt = new Scanner(System.in);		
			if(exibeSubmissoes()){
				System.out.printf("Digite o c�digo da submiss�o que deseja aceitar: ");
				indice = tecInt.nextInt();
				
				Atividade ativ = submissoes.get(indice);
				ativ.getRecursosNec();
				
				// Define local e vagas
				System.out.printf("Defina o local da atividade (Ex.: Sala 1, Audit�rio, etc.): ");
				ativ.setLocal(tecString.nextLine());
				System.out.printf("N�mero de vagas: ");
				ativ.setTotalDeVagas(tecInt.nextInt());
				System.out.println("---- INFORMA��ES DE DATA E HORA  ----");
				System.out.printf("Data e hor�rio de in�cio (Ex.: 01/12/16 08:00) : ");
				ativ.setDataHoraIncio(tecString.nextLine());
				System.out.printf("Data e hor�rio de encerramento (Ex.: 01/12/16 08:00) : ");
				ativ.setDataHoraFim(tecString.nextLine());				

				//Persistencia
				decrementaEstoque(ativ,indice); //atualizar estoque de recursos dispon�veis
				Dados.salvaRecursos(this.getRecursosDisponiveis());
				atividades.add(ativ);
				Dados.salvaSubmissoesAprovadas(ativ);//uma por vez
				submissoes.remove(ativ); //remove a atividade da lista de espera
				Dados.salvaSubmissoesAguardandoAprovacao(this.submissoes);

				System.out.println("\nA submiss�o foi aceita!\n");
				Principal.menuGerenciaEvento();
			}else{
				System.out.println("---------| ATEN��O |---------");
				System.out.println("A lista est� vazia!\n");
				Principal.menuGerenciaSubmissoes();
			}

		}catch(Exception e){
			System.out.println("\n ----| ERRO! |----\n Tente novamente!\n");
			aprovaSubmissao();
		}
	}
		

	public Pessoa inscricaoGenerica(Pessoa pessoa) throws IOException {
		
		Evento.setTotalInscritos(totalInscritos);// incrementa total de incritos
		verificaVagasDisponives(); // decrementa vagas disponiviveis para o evento principal
		
		Scanner tecInt = new Scanner(System.in);
		// Nome e apelido
		
		System.out.printf("Nome Completo: ");
		pessoa.setNome(tecString.nextLine());
		System.out.printf("Apelido (Nome para ser impresso no crach�): ");
		pessoa.setApelido(tecString.nextLine());

		// Forma��o e biografia
		System.out.printf("Forma��o acad�mica (Ex.: Mestre em Seguran�a da Informa��o - USP): ");
		pessoa.setFormacaoAcademica(tecString.nextLine());

		// Contato
		System.out.printf("Telefone ((88) 9 0000-0000): ");
		pessoa.setTelefone(tecString.nextLine());
		System.out.printf("Email: ");
		pessoa.setEmail(tecString.nextLine());

		// Endereco
		System.out.printf("Cidade: ");
		pessoa.setCidade(tecString.nextLine());
		System.out.printf("Bairro: ");
		pessoa.setBairro(tecString.nextLine());
		System.out.printf("Logradouro: ");
		pessoa.setLogradouro(tecString.nextLine());
		System.out.printf("N�mero: ");
		pessoa.setNum(tecString.nextLine());

		return pessoa;

	}

	public void inscricaoParticipante() throws IOException {
		if (atividades.isEmpty()) {
			try {
				System.out.println("----------------------| ATEN��O |----------------------");
				System.out.println("Este evento ainda n�o tem nenhuma atividade cadastrada.\n"
						+ "deseja continuar sua inscri��o apenas como participante? 1- Sim | 2- N�o");
				System.out.printf("Resposta: ");
				int resp = tecInt.nextInt();
				if (resp == 2) {
					Principal.menuPrincipalPart();
				} else if (resp != 1) {
					System.out.println("\nVoc� digitou uma op��o inv�lida! Tente novamente!\n");
					Principal.menuPrincipalPart();
				}
			} catch (Exception e) {
				System.out.println("\nVoc� n�o digitou uma op��o inv�lida! Tente novamente!\n");
				inscricaoParticipante();

			}
		}
		Pessoa pessoa = new Pessoa();
		if (verificaVagasEvento() == true) {
			pessoa = inscricaoGenerica(pessoa);
			pessoa.setTipoParticipante("Participante");
			pessoa.setBiografia("-"); // Participante n�o tem biografia

			Dados.salvaInscrito(pessoa);
			// setParticipantes(Dados.carregaInscritos());

			System.out.println("Voc� est� inscrito! N�o falte para receber seu certificado de participa��o!");
			System.out.println("Agora voc� ir� se inscrever na atividades do evento (Palestras, oficinas, etc.).\n"
					+ "Mas, aten��o voc� s� pode escolher at� 5 atividades, ent�o escolha muito bem!\n\n");

			int cont = 0, opcao = 1;

			try {
				do {
					System.out.println("Atividade: " + cont);
					System.out.println("Digite: \n" + "1: Continuar\n" + "2: Concluir Inscri��o\n"
							+ "3: Listar minhas atividades\n");
					opcao = tecInt.nextInt();
					System.out.println("Resposta: ");

					switch (opcao) {
					case 1:
						Atividade ativ = selecionaAtividade(); // seleciona a atividade que deseja se inscrever;
						if (verificaVagasAtividade(ativ, pessoa)) {
							cont++;
						}
						// verificaVagasAtividade(ativ, pessoa); // se houver vaga participante e reduz a quantidade de vagas disponiveis
						break;
					case 2:
						Principal.menuPrincipalPart();
						break;
					case 3:
						System.out.println("----| ATEN��O |----\n"
								+ "Anote as informa��es a seguir pois elas ser�o perdidas ap�s o fechamento do programa");
						pessoa.listaAtividadesDoInscrito();
						break;
					default:
						System.out.println("Op��o inv�lida! Tente novamente!");
						break;
					}

				} while (cont < 6 && opcao != 0);
				
				System.out.println("----| ATEN��O |----\n"
						+ "Anote as informa��es a seguir pois elas ser�o perdidas ap�s o fechamento do programa");
				pessoa.setNumInscricao(Evento.getTotalInscritos());
				System.out.printf("INSCRI��O N�: " + pessoa.getNumInscricao() + "\n");
				pessoa.listaAtividadesDoInscrito();
			}catch(Exception e){
				System.out.println("\n ----| ERRO! |----\n Tente novamente!\n");
				inscricaoParticipante();
			}		
		}	
	}

	// *FALTA* verificar se o evento j� foi cadastrado
	public boolean verificaVagasAtividade(Atividade ativ, Pessoa pessoa) throws IOException {

		if (ativ.getVagasDisponives() > 0) {
			ativ.setVagasDisponives();
			adicionaParticipanteNaAtividade(pessoa);

			Dados.salvaAtividadeDoInscrito(ativ);
			ativ.setAtividades(Dados.carregaAtividadesdoInscrito());

			System.out.println("Voc� se inscreve na " + ativ.getTipo() + " " + ativ.getNome() + "!");
			return true; // se tiver vaga
		} else {
			System.out.println("-----------------| ATEN��O |-----------------\n"
					+ "Sua inscri��o n�o pode ser realizada\n" + "N�o h� mais nenhuma vaga dispon�vel em "
					+ ativ.getNome() + "\n" + "---------------------------------------------");
			return false; // se n�o tiver vaga
		}
	}		
	
	public void exibeInscritosEvento() throws IOException{
		if(!participantes.isEmpty()){
			for(Pessoa p : participantes){
				System.out.println("N� INCRI��O: " + p.getNumInscricao() +
						" | Nome: " + p.getNome() +
						" | Apelido: " + p.getApelido() +
						" | Email: " + p.getEmail() +
						" | Tipo: " + p.getTipoParticipante());
			}
			Principal.menuGerenciaEvento();
		}else{
			System.out.println("\nNenhum Participante cadastrado!\n");
			Principal.menuGerenciaEvento();
		}
	}
	
	public void adicionaParticipanteNaAtividade(Pessoa pessoa) {
		participantes.add(pessoa);
	}

	public Atividade selecionaAtividade() throws IOException {
		exibeAtividadesAprovadas();

		System.out.print("Digite o c�digo da atividade: ");
		int i = tecInt.nextInt();
		Atividade a = atividades.get(i);

		return a;
	}

	/*
	public void exibeAtividades() {
		for (Atividade a : atividades) {
			System.out.println(a);
			asdfas
		}
	}
	*/
	// *CRIADO* Este m�todo exibe os recursos CADASTRADOS para o evento
	public void exibeRecursos() throws IOException {
		Evento ev = carregaEvento();
		try {
			System.out.println("----| LISTA DE RECURSOS |----");
			for (Recurso r : ev.recursosDisponiveis) {
				System.out.println(ev.recursosDisponiveis.indexOf(r) + " - " + r.getNome() + " - " + r.getQuantidade());
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Nenhum recurso foi cadastrado!!!!!!!!!!");
			Principal.menuGerenciaRecurso();
		}
	}
	
	public void exibeTodosOsRecursos() throws IOException {
		try {
			System.out.println("----| LISTA DE RECURSOS |----");
			for (Recurso r : this.recursosDisponiveis) {
				System.out.println(this.recursosDisponiveis.indexOf(r) + " - " + r.getNome() + " - " + r.getQuantidade());
			}
			Principal.menuGerenciaRecurso();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("Nenhum recurso foi cadastrado!!!!!!!!!!");
			Principal.menuGerenciaRecurso();
		}
	}

	// *CRIADO* M�todo para seleciona o recurso que o ministrante deseja usar na
	// sua palestra
	public Recurso selecionaRecurso() throws IOException {
		Evento ev = carregaEvento();
		exibeRecursos();
		
		System.out.print("Digite o c�digo do recurso: ");
		int i = tecInt.nextInt();
		
		Recurso r = ev.recursosDisponiveis.get(i);

		return r;
	}

	// M�todos Getters E Setters
	
	//*CRIADO* Calcula o total de vagas que est�o sobrando
	//*FALTANDO* Assim funciona, mas devemos criar uma exception 
	public void setVagasDisponives() {
		if (this.vagasDisponives > 0){
			this.vagasDisponives--;
		}else{
			System.out.println("aten��o -- verifique a quantidade de vagas disponiveis");
		}
					
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCoordenador() {
		return coordenador;
	}

	public void setCoordenador(String coordenador) {
		this.coordenador = coordenador;
	}

	public String getDataHoraIncio() {
		return dataHoraIncio;
	}

	public void setDataHoraIncio(String dataHoraIncio) {
		this.dataHoraIncio = dataHoraIncio;
	}

	public String getDataHoraFim() {
		return dataHoraFim;
	}

	public void setDataHoraFim(String dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int i) {
		this.num = i;
	}

	public ArrayList<Pessoa> getParticipantes() {
		return participantes;
	}

	public void setParticipantes(ArrayList<Pessoa> participantes) {
		this.participantes = participantes;
	}

	public int getTotalDeVagas() {
		return totalDeVagas;
	}

	public void setTotalDeVagas(int totalDeVagas) {
		this.totalDeVagas = totalDeVagas - Evento.totalInscritos;
	}

	public ArrayList<Recurso> getRecursosDisponiveis() {
		return recursosDisponiveis;
	}

	public void setRecursosDisponiveis(ArrayList<Recurso> recursos) {
		this.recursosDisponiveis = recursos;
	}

	public ArrayList<Atividade> getAtividades() {
		return atividades;
	}

	public void setAtividades(ArrayList<Atividade> atividades) {
		this.atividades = atividades;
	}

	public static int getTotalInscritos() {
		return totalInscritos;
	}

	public static void setTotalInscritos(int totalInscritos) {
		Evento.totalInscritos++;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public int getVagasDisponives() {
		return vagasDisponives;
	}

	public void setVagasDisponiveis() {
		this.vagasDisponives = totalDeVagas;
	}

	// *CRIADO* Calcula o total de vagas que est�o sobrando
	// *FALTANDO* Assim funciona, mas devemos criar uma exception
	public void verificaVagasDisponives() {
		if (this.vagasDisponives > 0) {
			this.vagasDisponives--;
		} else {
			System.out.println("aten��o -- verifique a quantidade de vagas disponiveis");
		}

	}

	public ArrayList<Atividade> getSubmissoes() {
		return submissoes;
	}

	public void setSubmissoes(ArrayList<Atividade> submissoes) {
		this.submissoes = submissoes;
	}

	public void verificaVagasDisponives(int vagasDisponives) {
		this.vagasDisponives = vagasDisponives;
	}

	@Override
	public String toString() {
		return getNome() + "|" + getDescricao() + "|" + getCoordenador() + "|" + getLocal() + "|" + getTipo() + "|"
				+ getTotalDeVagas() + "|" + getTotalInscritos() + "|" + getVagasDisponives() + "|" + getDataHoraIncio()
				+ "|" + getDataHoraFim() + "|" + getCidade() + "|" + getBairro() + "|" + getLogradouro() + "|"
				+ getNum();

	}

}
