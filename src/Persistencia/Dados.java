package Persistencia;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.regex.Pattern;

import Evento.Atividade;
import Evento.Evento;
import Evento.Pessoa;
import Evento.Recurso;


public class Dados {
	
	//*PRONTO* Salva Recursos Cadastrados no EVENTO PRINCIPAL
	public static void salvaRecursos (Recurso recurso) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Recursos.txt",true);
        PrintWriter gravarArquivo = new PrintWriter(arquivo);
        
        gravarArquivo.println(recurso); // imprime o produto no arquivo
        arquivo.flush(); //libera a grava�ao
        arquivo.close(); //fecha o arquivo
	}
	//*OBSERVA��O* sobrecarga que salva o arraylist inteiro ao inv�s de apenas um recuso
	public static void salvaRecursos (ArrayList<Recurso> recursos) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Recursos.txt",false);
        PrintWriter gravarArquivo = new PrintWriter(arquivo);
        
        for(Recurso r : recursos){
        	gravarArquivo.println(r.getNome() + "|" + r.getQuantidade());
        	
        }
   
        arquivo.flush(); //libera a grava�ao
        arquivo.close(); //fecha o arquivo
	}
	
	//*PRONTO* Carrega Recursos Cadastrados no EVENTO PRINCIPAL
	public static ArrayList<Recurso> carregaRecursos() throws IOException {
        FileReader arq = null;
        ArrayList<Recurso> listaRecursos = null;
        try{
        	arq = new FileReader("cad_Recursos.txt");
        	listaRecursos = new ArrayList<Recurso>();
            
            //armazenando conteudo no arquivo no buffer
            BufferedReader lerArq = new BufferedReader(arq);
            
            //lendo a primeira linha
            String linha = lerArq.readLine();
            
            //a variavel linha recebe o valor 'null' quando chegar no final do arquivo
            while (linha != null){
            	Recurso r = new Recurso();
                String[] atributos = linha.split(Pattern.quote("|"));
                //passa os "atributos" do Array para o objeto p
                r.setNome(atributos[0]);
                r.setQuantidade(Integer.parseInt(atributos[1]));
                //Adiciona o objeto p na ArrayList produtos
                listaRecursos.add(r);
                //Captura a pr�xima linha
                linha = lerArq.readLine();

            }
            lerArq.close();
    	   return listaRecursos;
        }catch(Exception e){
        	System.out.println(e.getMessage() + " | N�o carregou os recusos diponiveis do evento");
        	
        }
        return listaRecursos;
        
	    
	    
	  }

	//*PRONTO* Salvar EVENTO PRINCIPAL
	public static void salvaEvento(Evento evento) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Evento_Principal.txt",false);
        PrintWriter gravarArquivo = new PrintWriter(arquivo);
        
        gravarArquivo.println(evento); // imprime o evento no arquivo
        arquivo.flush(); //libera a grava�ao
        arquivo.close(); //fecha o arquivo
	}
	
	//*PRONTO* Carrega EVENTO PRINCIPAL
	public static Evento carregaEvento() throws IOException{
		FileReader arq = null;
		Evento evento = null;
		try{
			arq = new FileReader("cad_Evento_Principal.txt");
			evento = new Evento();
	        
			//armazenando conteudo no arquivo no buffer
			BufferedReader lerArq = new BufferedReader(arq);
			//lendo a primeira linha
			String linha = lerArq.readLine();
			//a variavel linha recebe o valor 'null' quando chegar no final do arquivo
			int i = 0;
			while (linha != null){			
			    String[] atributos = linha.split(Pattern.quote("|"));
			    //passa os "atributos" do Array para o objeto p
				evento.setNome(atributos[0]);
				evento.setDescricao(atributos[1]);
				evento.setCoordenador(atributos[2]);	 
				evento.setLocal(atributos[3]);
				evento.setTipo(atributos[4]);
				evento.setTotalDeVagas(Integer.parseInt(atributos[5]));
				Evento.setTotalInscritos(Integer.parseInt(atributos[6]));
				evento.setVagasDisponiveis();
				evento.setDataHoraIncio(atributos[8]);
				evento.setDataHoraFim(atributos[9]);
				evento.setCidade(atributos[10]);
				evento.setBairro(atributos[11]);
				evento.setLogradouro(atributos[12]);
				evento.setNum(Integer.parseInt(atributos[13]));
				
			    //Captura a pr�xima linha
			    linha = lerArq.readLine();

			}
				lerArq.close();
				return evento;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return evento;
	        
	}
	
	//*CRIADO* Salva pessoas inscritas NO EVENTO PRINCIPAL
 	public static void salvaInscrito (Pessoa inscrito) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Inscrito_Evento_Principal.txt",true);
        PrintWriter gravarArquivo = new PrintWriter(arquivo);
        
        gravarArquivo.println(inscrito); // imprime o produto no arquivo
        arquivo.flush(); //libera a grava�ao
        arquivo.close(); //fecha o arquivo
	}
	
	//*CRIADO* Carrega pessoas inscritas NO EVENTO PRINCIPAL
	public static ArrayList<Pessoa> carregaInscritos() throws IOException {
		FileReader arq = null;
		ArrayList<Pessoa> listaInscritos = null;
		try{
			listaInscritos = new ArrayList<Pessoa>();
        	arq = new FileReader("cad_Inscrito_Evento_Principal.txt");
            //armazenando conteudo no arquivo no buffer
            BufferedReader lerArq = new BufferedReader(arq);      
            //lendo a primeira linha
            String linha = lerArq.readLine();
            //a variavel linha recebe o valor 'null' quando chegar no final do arquivo
            while (linha != null){
            	Pessoa p = new Pessoa();
                String[] atributos = linha.split(Pattern.quote("|"));
                //passa os "atributos" do Array para o objeto p
                p.setNumInscricao(Integer.parseInt(atributos[0]));
                p.setNome(atributos[1]);
                p.setApelido(atributos[2]);
                p.setEmail(atributos[3]);
                p.setTelefone(atributos[4]);
                p.setTipoParticipante(atributos[5]);
                p.setBiografia(atributos[6]);
                p.setCidade(atributos[7]);
                p.setBairro(atributos[8]);
                p.setLogradouro(atributos[9]);
                p.setNum(atributos[10]);
                //Adiciona o objeto p na ArrayList produtos
                listaInscritos.add(p);
                //Captura a pr�xima linha
                linha = lerArq.readLine();

            }
            lerArq.close();
            return listaInscritos;
        }catch(Exception e){
        	System.out.println(e.getMessage());
        }
		return listaInscritos;
	  }
	
	//*CRIADO* Salva atividade do inscrito
	public static void salvaAtividadeDoInscrito (Atividade atividade) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Atividades_Do_Inscrito.txt",true);
        PrintWriter gravarArquivo = new PrintWriter(arquivo);
        
        gravarArquivo.println(atividade); // imprime o produto no arquivo
        arquivo.flush(); //libera a grava�ao
        arquivo.close(); //fecha o arquivo
	}
	
	//*CRIADO* Carrega ativadades do inscrito
	public static ArrayList<Atividade> carregaAtividadesdoInscrito() throws IOException{
		FileReader arq = null;
		ArrayList<Atividade> listaAtividades = null;
		try{
			arq = new FileReader("cad_Atividades_Do_Inscrito.txt");
			listaAtividades = new ArrayList<Atividade>();
	        
	        //armazenando conteudo no arquivo no buffer
	        BufferedReader lerArq = new BufferedReader(arq);
	        
	        //lendo a primeira linha
	        String linha = lerArq.readLine();
	        
	        //a variavel linha recebe o valor 'null' quando chegar no final do arquivo
	        while (linha != null){
	        	Atividade a = new Atividade();
	            String[] atributos = linha.split(Pattern.quote("|"));
	            //passa os "atributos" do Array para o objeto a
	            Atividade.setId(Integer.parseInt(atributos[0]));
	            a.setNome(atributos[1]);
	            a.setDescricao(atributos[2]);
	            a.setMinistrante(atributos[3]);
	            a.setLocal(atributos[4]);
	            a.setTipo(atributos[5]);
	            a.setTotalDeVagas(Integer.parseInt(atributos[6]));
	            Atividade.setTotalInscritos(Integer.parseInt(atributos[7]));
	            a.setVagasDisponiveis();
	            a.setDataHoraIncio(atributos[9]);
	            a.setDataHoraFim(atributos[10]);
	          
	            //Adiciona o objeto p na ArrayList produtos
	            listaAtividades.add(a);
	            //Captura a pr�xima linha
	            linha = lerArq.readLine();

	        }
	        lerArq.close();
	        
		   return listaAtividades;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return listaAtividades;
        
	}

	//*CRIADO* Salva submissoes ques estao aguardando aprova��o
	public static void salvaSubmissoesAguardandoAprovacao (Atividade atividade) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Submissoes_Aguardando_Aprovacao.txt",true);
	    PrintWriter gravarArquivo = new PrintWriter(arquivo);
	    
	    gravarArquivo.println(atividade); // imprime o produto no arquivo
	    arquivo.flush(); //libera a grava�ao
	    arquivo.close(); //fecha o arquivo
	}
	//*OBSERVA��O* sobrecarga
	public static void salvaSubmissoesAguardandoAprovacao (ArrayList<Atividade> atividades) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Submissoes_Aguardando_Aprovacao.txt",false);
	    PrintWriter gravarArquivo = new PrintWriter(arquivo);
	    
	    for(Atividade a : atividades){
	    	gravarArquivo.println(a); // imprime o produto no arquivo
	    }
	    
	    arquivo.flush(); //libera a grava�ao
	    arquivo.close(); //fecha o arquivo
	}
	
	
	//*CRIADO* Carrega submissoes que estao aguardando aprova��o
	public static ArrayList<Atividade> carregaSubmissoesAguardandoAprovacao() throws IOException{
		FileReader arq = null;
		BufferedReader lerArq = null;
		ArrayList<Atividade> listaSubmissoes = null;
		try{
			arq = new FileReader("cad_Submissoes_Aguardando_Aprovacao.txt");
			listaSubmissoes = new ArrayList<Atividade>();
		    
		    //armazenando conteudo no arquivo no buffer
		    lerArq = new BufferedReader(arq);
		    
		    //lendo a primeira linha
		    String linha = lerArq.readLine();
		    
		    //a variavel linha recebe o valor 'null' quando chegar no final do arquivo
		    while (linha != null){
		    	Atividade a = new Atividade();
		        String[] atributos = linha.split(Pattern.quote("|"));
		        //passa os "atributos" do Array para o objeto a
		        a.setId(Integer.parseInt(atributos[0]));
		        a.setNome(atributos[1]);
		        a.setDescricao(atributos[2]);
		        a.setMinistrante(atributos[3]);
		        a.setLocal(atributos[4]);
		        a.setTipo(atributos[5]);
		        a.setTotalDeVagas(Integer.parseInt(atributos[6]));
		        Atividade.setTotalInscritos(Integer.parseInt(atributos[7]));
		        a.setVagasDisponiveis();
		        a.setDataHoraIncio(atributos[9]);
		        a.setDataHoraFim(atributos[10]);
		        
				a.setRecursosNec(carregaRecursosAtividade(a)); //GAMBIARRA?!
				
		        //Adiciona o objeto p na ArrayList produtos
		        listaSubmissoes.add(a);
		        //Captura a pr�xima linha
		        linha = lerArq.readLine();
		
		    }
		    lerArq.close();
		    
		   return listaSubmissoes;
		}catch(Exception e){
			System.out.println(e.getMessage());
			
		}
		return listaSubmissoes;
	   
	}

	//*CRIADO* Salva submissoes aprovadas
	public static void salvaSubmissoesAprovadas (Atividade atividade) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Submissoes_Aprovadas.txt",true);
	    PrintWriter gravarArquivo = new PrintWriter(arquivo);
	    
	    gravarArquivo.println(atividade); // imprime o produto no arquivo
	    arquivo.flush(); //libera a grava�ao
	    arquivo.close(); //fecha o arquivo
	}
	//*OBSERVA��O* sobrecarga salva o arraylist inteiro
	public static void salvaSubmissoesAprovadas (ArrayList<Atividade> atividades) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Submissoes_Aprovadas.txt",false);
	    PrintWriter gravarArquivo = new PrintWriter(arquivo);
	 
	    for(Atividade a : atividades){
	    	gravarArquivo.println(a);//grava no arquivo
	    }
	    
	    arquivo.flush(); //libera a grava�ao
	    arquivo.close(); //fecha o arquivo
	}
	
	//*CRIADO* Carrega submissoes APROVADAS
	public static ArrayList<Atividade> carregaSubmissoesAprovadas() throws IOException{
		FileReader arq = null;
		BufferedReader lerArq = null;
		ArrayList<Atividade> listaAtividadesAprovadas = null;
		try{
			arq = new FileReader("cad_Submissoes_Aprovadas.txt");
			listaAtividadesAprovadas = new ArrayList<Atividade>();
		    
		    //armazenando conteudo no arquivo no buffer
		    lerArq = new BufferedReader(arq);
		    
		    //lendo a primeira linha
		    String linha = lerArq.readLine();
		    
		    //a variavel linha recebe o valor 'null' quando chegar no final do arquivo
		    while (linha != null){
		    	Atividade a = new Atividade();
		        String[] atributos = linha.split(Pattern.quote("|"));
		        //passa os "atributos" do Array para o objeto a
		        Atividade.setId(Integer.parseInt(atributos[0])); //PODE ACONTECER PROBLEMAS AQUI !!!!!!!
		        a.setNome(atributos[1]);
		        a.setDescricao(atributos[2]);
		        a.setMinistrante(atributos[3]);
		        a.setLocal(atributos[4]);
		        a.setTipo(atributos[5]);
		        a.setTotalDeVagas(Integer.parseInt(atributos[6]));
		        Atividade.setTotalInscritos(Integer.parseInt(atributos[7]));
		        a.setVagasDisponiveis();
		        a.setDataHoraIncio(atributos[8]);
		        a.setDataHoraFim(atributos[9]);
		        a.setRecursosPendentes(carregaRecursosPendentesAtividade(a));//falta criar o m�todo do parametro
		        a.setRecursosNec(carregaRecursosAtividade(a));
		      
		        //Adiciona o objeto p na ArrayList produtos
		        listaAtividadesAprovadas.add(a);
		        //Captura a pr�xima linha
		        linha = lerArq.readLine();
		
		    }
		    lerArq.close();
		    
		   return listaAtividadesAprovadas;
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return listaAtividadesAprovadas;
		}

	//*CRIADO* Salva os recursos de cada atividade usando seu ID como referencia
	public static void salvaRecursosAtividade (ArrayList<Recurso> recursos, Atividade ativ) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Recursos_Atividade.txt",true);
        PrintWriter gravarArquivo = new PrintWriter(arquivo);
        
        for(Recurso r : recursos){
        	gravarArquivo.println(ativ.getId() + "|" + r.getNome() + "|" + r.getQuantidade());
        }
        arquivo.flush(); //libera a grava�ao
        arquivo.close(); //fecha o arquivo
	}
		
	public static ArrayList<Recurso> carregaRecursosAtividade(Atividade ativ) throws IOException{
		FileReader arq = new FileReader("cad_Recursos_Atividade.txt");
		BufferedReader lerArq = null;
	    
	    //armazenando conteudo no arquivo no buffer
	    lerArq = new BufferedReader(arq);
	    
	    //ArrayList<Atividade> listaAtividades = new ArrayList<Atividade>(); 
	    
	    //lendo a primeira linha
	    String linha = lerArq.readLine();
	    ArrayList<Recurso> recursosAtividade = new ArrayList<Recurso>();
	    int idAtual;
	    //a variavel linha recebe o valor 'null' quando chegar no final do arquivo
	    while (linha != null){
	    	String[] atributos = linha.split(Pattern.quote("|"));
	    	idAtual = Integer.parseInt(atributos[0]);	    	
	    	if(ativ.getId() == idAtual){
	    		Recurso r = new Recurso();
	    		r.setNome(atributos[1]);
	    		r.setQuantidade(Integer.parseInt(atributos[2]));
	    		recursosAtividade.add(r);
	    	}
	    	linha = lerArq.readLine();
	    	idAtual = Integer.parseInt(atributos[0]);
    	}	    	
    	lerArq.close();
    	return recursosAtividade;
	}
	
	public static void salvaRecursosPendentesAtividade (ArrayList<Recurso> recursos, Atividade ativ) throws IOException{
		FileWriter arquivo = new FileWriter("cad_Recursos_Pendentes_Atividade.txt",true);
        PrintWriter gravarArquivo = new PrintWriter(arquivo);
        
        for(Recurso r : recursos){
        	gravarArquivo.println(ativ.getId() + "|" + r.getNome() + "|" + r.getQuantidade());
        }
        arquivo.flush(); //libera a grava�ao
        arquivo.close(); //fecha o arquivo
	}
	
	public static ArrayList<Recurso> carregaRecursosPendentesAtividade(Atividade ativ) throws IOException{
		FileReader arq = new FileReader("cad_Recursos_Pendentes_Atividade.txt");
		BufferedReader lerArq = null;
	    
	    //armazenando conteudo no arquivo no buffer
	    lerArq = new BufferedReader(arq);
	    
	    //ArrayList<Atividade> listaAtividades = new ArrayList<Atividade>(); 
	    
	    //lendo a primeira linha
	    String linha = lerArq.readLine();
	    ArrayList<Recurso> recursosPendentes = new ArrayList<Recurso>();
	    int idAtual;
	    //a variavel linha recebe o valor 'null' quando chegar no final do arquivo
	    while (linha != null){
	    	String[] atributos = linha.split(Pattern.quote("|"));
	    	idAtual = Integer.parseInt(atributos[0]);	    	
	    	if(ativ.getId() == idAtual){
	    		Recurso r = new Recurso();
	    		r.setNome(atributos[1]);
	    		r.setQuantidade(Integer.parseInt(atributos[2]));
	    		recursosPendentes.add(r);
	    	}
	    	linha = lerArq.readLine();
	    	idAtual = Integer.parseInt(atributos[0]);
    	}	    	
    	lerArq.close();
    	return recursosPendentes;
	}
		
}
